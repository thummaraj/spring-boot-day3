package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.Manufacturer
import dv.spring.kotlin.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository


@Repository
class AddressDaoDBlmpl: AddressDao {
    override fun findById(id: Long): Address? {
        return addressRepository.findById(id).orElse(null)
    }

    @Autowired
    lateinit var addressRepository: AddressRepository
    override fun save(address: Address): Address {
        return addressRepository.save(address)
    }

}