package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBlmpl: CustomerDao {
    override fun findById(id: Long): Customer? {
       return customerRepository.findById(id).orElse(null)
    }

    override fun findByStatus(status: String): List<Customer> {
        try {
            return customerRepository.findByUserStatus(UserStatus.valueOf(status.toUpperCase()))
        }catch (e: IllegalArgumentException){
            return emptyList()
        }
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository
    override fun save(customer: Customer): Customer {
        return customerRepository.save(customer)
    }

    override fun getWhoseByAddress(name: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase((name))
    }

    override fun getCustomerByNameOrEmail(name: String, email: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name,email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerRepository.findCustomerByNameEndingWithIgnoreCase(name)
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerRepository.findCustomerByName(name)
    }

    override fun getCustomer(): List<Customer> {
//        return customerRepository.findAll().filterIsInstance(Customer::class.java)
        return customerRepository.findByIsDeletedIsFalse()
    }
}