package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductDao {
     fun getSelectedProductWithProductName(name: String, page: Int, pageSize: Int): Page<SelectedProduct>
}