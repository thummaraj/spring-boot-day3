package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartDao {
    fun getShoppingCart(): List<ShoppingCart>
    fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
     fun getShoppingCartWithProductName(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
     fun findByProductName(name: String): List<ShoppingCart>
     fun save(shoppingCart: ShoppingCart): ShoppingCart

}