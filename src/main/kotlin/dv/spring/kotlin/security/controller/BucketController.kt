//package dv.spring.kotlin.security.controller
//
//import dv.spring.kotlin.security.service.AmazonClient
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.DeleteMapping
//import org.springframework.web.bind.annotation.PostMapping
//import org.springframework.web.bind.annotation.RequestPart
//import org.springframework.web.bind.annotation.RestController
//import org.springframework.web.multipart.MultipartFile
//
//@RestController
//class BucketController {
//    @Autowired
//    lateinit var amazonClient: AmazonClient
//
//    @PostMapping("/uploadFile")
//    fun uploadFile(@RequestPart(value = "file") file: MultipartFile): ResponseEntity<*>{
//        return ResponseEntity.ok(this.amazonClient.uploadFile(file))
//    }
//    @DeleteMapping("/deleteFile")
//    fun deleteFile(@RequestPart(value = "url") fileUrl:String): ResponseEntity<*>{
//        return ResponseEntity.ok(this.amazonClient.deleteFileFromS3Bucket(fileUrl))
//    }
//}