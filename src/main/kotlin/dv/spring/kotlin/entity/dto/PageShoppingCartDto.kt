package dv.spring.kotlin.entity.dto

data class PageShoppingCartDto (var totalPages: Int,
                                var totalElements: Long,
                                var shoppingList: List<ShoppingCustomerDto>)