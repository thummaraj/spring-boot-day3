package dv.spring.kotlin.service

import dv.spring.kotlin.dao.SelectedProductDao
import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SelectedProductServicelmpl: SelectedProductService {
    @Autowired
    lateinit var selectedProductDao: SelectedProductDao
    override fun getSelectedProductWithProductName(name: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductDao.getSelectedProductWithProductName(name,page,pageSize)
    }
}