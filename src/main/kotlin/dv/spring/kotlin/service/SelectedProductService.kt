package dv.spring.kotlin.service

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductService {
    fun getSelectedProductWithProductName(name: String, page: Int, pageSize: Int): Page<SelectedProduct>
}