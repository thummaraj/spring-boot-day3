package dv.spring.kotlin.util

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.entity.dto.*
import dv.spring.kotlin.security.entity.Authority
import org.mapstruct.*
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
    @Mappings(Mapping(source = "manufacturer",target = "manu"))
    fun mapProductDto(product: Product?):ProductDto?
    fun mapProductDto(products: List<Product>):List<ProductDto>
    fun mapManufacturer(manu: Manufacturer): ManufacturerDto
    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer
    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto): Product

    @Mappings(
           Mapping(source = "customer.jwtUser.username", target = "username"),
            Mapping(source = "customer.jwtUser.authorities", target = "authorities"),
            Mapping(source = "defaultAddress", target = "address")
    )
    fun mapUser(customer: Customer): UserDto
    fun mapCustomerDto(customer: Customer?):CustomerDto?
    fun mapCustomerDto(customers: List<Customer>):List<CustomerDto>

    @InheritInverseConfiguration
    fun mapCustomerDto(customerDto: CustomerDto): Customer
    fun mapAddress(address: Address): AddressDto
    @InheritInverseConfiguration
    fun mapAddress(address: AddressDto): Address

    @Mappings(Mapping(source = "selectedProducts", target = "selectedProducts"))
    fun mapShoppingCartDto(shoppingCart: ShoppingCart): ShoppingCartDto
    fun mapShoppingCartDto(shoppingCarts: List<ShoppingCart>): List<ShoppingCartDto>
    @InheritInverseConfiguration
    fun mapShoppingCartDto(shoppingCartDto: ShoppingCartDto): ShoppingCart
     fun mapSelectedProductDto(selectedProducts: List<SelectedProduct>): List<SelectedProductDto>

    @Mappings(
            Mapping(source = "product.name", target = "name"),
            Mapping(source = "product.description", target = "description"),
            Mapping(source = "quantity", target = "quantity")
    )
    fun mapProductDisplay(selectedProduct: SelectedProduct): DisplayProduct
    fun mapProductDisplay(selectedProducts: List<SelectedProduct>): List<DisplayProduct>

    @Mappings(
            Mapping(source = "customer.name", target = "customer"),
            Mapping(source = "customer.defaultAddress", target = "address"),
            Mapping(source = "selectedProducts", target = "products")
    )
    fun mapShoppingCartCustomerDto(shoppingCart: ShoppingCart): ShoppingCustomerDto
    fun mapShoppingCartCustomerDto(shoppingCarts: List<ShoppingCart>): List<ShoppingCustomerDto>
    @InheritInverseConfiguration
    fun mapShoppingCartCustomerDto(shoppingCart: ShoppingCustomerDto): ShoppingCart


    fun mapAuthority(authority: Authority): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>







}