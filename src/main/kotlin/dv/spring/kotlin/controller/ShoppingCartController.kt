package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.PageShoppingCartDto
import dv.spring.kotlin.entity.dto.ShoppingCartDto
import dv.spring.kotlin.entity.dto.ShoppingCustomerDto
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService
    @GetMapping("/shoppingCart")
    fun  getShoppingCart(): ResponseEntity<Any>{
        val shoppingCarts = shoppingCartService.getShoppingCart()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCarts))
    }
    @GetMapping("/allshoppingCart")
    fun getShoppingCartWithPage(@RequestParam("page") page:Int,
                               @RequestParam("pageSize") pageSize:Int): ResponseEntity<Any>{
        val output = shoppingCartService.getShoppingCartWithPage(page,pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingList = MapperUtil.INSTANCE.mapShoppingCartCustomerDto(output.content)))
    }
    @GetMapping("/shoppingCart/productName")
    fun getShoppingCartWithProductName(@RequestParam("name") name: String,
            @RequestParam("page") page:Int,
                                @RequestParam("pageSize") pageSize:Int): ResponseEntity<Any>{
        val output = shoppingCartService.getShoppingCartWithProductName(name,page,pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                shoppingList = MapperUtil.INSTANCE.mapShoppingCartCustomerDto(output.content)))
    }
    @PostMapping("/addShoppingCart")
    fun addShoppingCart(@RequestBody shoppingCartDto: ShoppingCustomerDto): ResponseEntity<Any>{
        val output = shoppingCartService.save(MapperUtil.INSTANCE.mapShoppingCartCustomerDto(shoppingCartDto))
        val outputDto = MapperUtil.INSTANCE.mapShoppingCartCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

}