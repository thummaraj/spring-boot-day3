package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import org.springframework.data.repository.CrudRepository

interface CustomerRepository: CrudRepository<Customer,Long> {
    fun findCustomerByName(name: String): Customer?
    fun findCustomerByNameEndingWithIgnoreCase(name: String): List<Customer>
    fun findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name: String, email:String): List<Customer>
    fun findByDefaultAddress_ProvinceContainingIgnoreCase(name: String): List<Customer>
    fun findByUserStatus(status: UserStatus): List<Customer>
    fun findByIsDeletedIsFalse(): List<Customer>
}