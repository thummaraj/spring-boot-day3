package dv.spring.kotlin.config

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.repository.*
import dv.spring.kotlin.security.entity.Authority
import dv.spring.kotlin.security.entity.AuthorityName
import dv.spring.kotlin.security.entity.JwtUser
import dv.spring.kotlin.security.repository.AuthorityRepository
import dv.spring.kotlin.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner{
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
    @Autowired
    lateinit var dataLoader: DataLoader
    @Autowired
    lateinit var authorityRepository : AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Transactional
    override fun run(args: ApplicationArguments?) {
        var manu1 = manufacturerRepository.save(Manufacturer("CAMT","0000000"))
        var manu2 = manufacturerRepository.save(Manufacturer("SAMSUNG","555666777888"))
        var manu3 = manufacturerRepository.save(Manufacturer("Apple","053123456"))
        var product1 = productRepository.save(Product("CAMT","The best College in CMU",
                0.0,1,"http://www.camt.cmu.ac.th/th/images/logo.jpg"))
        var product2 = productRepository.save(Product("iPhone", "It's a phone",
                28000.0, 20, "https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))
        var product3 = productRepository.save(Product("Note 9", "Other Iphone",
                28001.0, 10, "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))
        var product4 = productRepository.save(Product("CAMT", "The best College in CMU",
                0.00, 1, "http://www.camt.cmu.ac.th/th/images/logo.jpg"))
        var product5 = productRepository.save(Product("Prayuth", "The best PM ever",
                1.0, 1, "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))

        manu1.products.add(product1)
        product1.manufacturer = manu1
        manu3.products.add(product2)
        product2.manufacturer = manu3
        manu2.products.add(product3)
        product3.manufacturer = manu2
        manu1.products.add(product4)
        product4.manufacturer = manu1
        manu1.products.add(product5)
        product5.manufacturer = manu1

        var custom1 = customerRepository.save(Customer("Lung","pm@go.th",UserStatus.ACTIVE))
        var custom2 = customerRepository.save(Customer("Chatchart","chut@taopoon.com",UserStatus.ACTIVE))
        var custom3 = customerRepository.save(Customer("Thanathon","thanathorn@life.com",UserStatus.PENDING))
        var addr1 = addressRepository.save(Address("Anutsawaree","Dinsaw","Dusit","bangkok","10123"))
        var addr2 = addressRepository.save(Address("239 \"ChiangMai U ","Suthep","Muang","CM","50200"))
        var addr3 = addressRepository.save(Address("Earth","Suksan","Muang","Konkein","12457"))

        custom1.defaultAddress = addr1
        custom2.defaultAddress = addr2
        custom3.defaultAddress = addr3

        var select1 = selectedProductRepository.save(SelectedProduct(4,product2))
        var select2 = selectedProductRepository.save(SelectedProduct(1,product5))
        var select3 = selectedProductRepository.save(SelectedProduct(1,product5))
        var select4 = selectedProductRepository.save(SelectedProduct(1,product3))
        var select5 = selectedProductRepository.save(SelectedProduct(2,product4))

        var shoppingCart1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        var shoppingCart2 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))

        shoppingCart1.customer = custom1
        shoppingCart2.customer = custom2

        shoppingCart1.selectedProducts.add(select1)
        shoppingCart1.selectedProducts.add(select2)
        shoppingCart2.selectedProducts.add(select3)
        shoppingCart2.selectedProducts.add(select4)
        shoppingCart2.selectedProducts.add(select5)

        @Transactional
        fun loadUsernameAndPassword(){
            val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
            val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
            val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
            authorityRepository.save(auth1)
            authorityRepository.save(auth2)
            authorityRepository.save(auth3)
            val encoder = BCryptPasswordEncoder()
            val cust1 = Customer(name = "Somchat", email = "a@b.com")
            val custJwt = JwtUser(
                    username = "customer",
                    password = encoder.encode("password"),
                    email = cust1.email,
                    enabled = true,
                    firstname = cust1.name,
                    lastname = "unknown"
            )
            customerRepository.save(cust1)
            userRepository.save(custJwt)
            cust1.jwtUser = custJwt
            custJwt.user = cust1
            custJwt.authorities.add(auth2)
            custJwt.authorities.add(auth3)

        }

        dataLoader.loadData()
        loadUsernameAndPassword()




    }
}